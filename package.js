Package.describe({
    name: 'molosc:tweener',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: 'Tweener for Meteor',
    // URL to the Git repository containing the source code for this package.
    git: 'https://Molosc@bitbucket.org/Molosc/tweener.git',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.1');
    api.use('ecmascript');
    api.addFiles(['tweener.js'], 'client');
    api.export('Tweener', 'client');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('molosc:tweener');
    api.addFiles('tweener-tests.js');
});
